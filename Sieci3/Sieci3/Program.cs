﻿using System;

namespace Sieci3
{
    class Program
    {
        static void SetArray(ref double[] vector0, ref double[] vector1)
        {
            Array.Fill(vector1, -1.0);
            Array.Fill(vector1, -1.0);

            vector0[6] = 1;
            vector0[7] = 1;
            vector0[8] = 1;
            vector0[11] = 1;
            vector0[13] = 1;
            vector0[16] = 1;
            vector0[17] = 1;
            vector0[18] = 1;
                   
            vector1[6] = 1;
            vector1[7] = 1;
            vector1[12] = 1;
            vector1[17] = 1;
        }

        static double ActivationFunction(double value)
        {
            if (value >= 0)
                return 1.0;
            return -1.0;
        }

        static void FillMatrix(double[] vector0, double[] vector1, ref double[,] matrix)
        {
            for (int i = 0; i < vector0.Length; i++)
            {
                for (int j = 0; j < vector1.Length; j++)
                    matrix[i, j] = 1 / 25.0 * (vector0[i] * vector0[j] + vector1[i] + vector1[j]);
            }
        }

        static double[] FFunction(double[,] matrix, double[] vector)
        {
            double[] SGNResult = new double[25];
            for(int i = 0; i < 25; i++)
            {
                double[] mathResult = new double[25]; 
                for(int j = 0; j < 25; j++)
                {
                    SGNResult[i] = ActivationFunction(vector[i] * matrix[i, j]);
                }
            }
            return SGNResult;
            // zwraca SNG z iloczynu skalarnego macierzy i wektora
        }

        static void VisualMartix(double[] martix)
        {
            for(int i = 0; i<25; i++)
            {
                for (int j = 0; j < 25; j++)
                {
                    Console.Write($"{martix[i,j]} ");
                }
                Console.Write("\n");
            }
        }

        static void Main(string[] args)
        {
            double[] vector0 = new double[25];
            double[] vector1 = new double[25];
            double[,] matrix = new double[25, 25];

            SetArray(ref vector0, ref vector1);
            FillMatrix(vector0, vector1, ref matrix);

            Console.WriteLine("This is first result:");
            double[] firstResult = FFunction(matrix, vector0);
            VisualMartix(firstResult);
            Console.WriteLine("This is second result:");
            double[] secondResult = FFunction(matrix, vector1);
            VisualMartix(secondResult);
            Console.WriteLine("Hello World!");
            Console.ReadLine();
        }
    }
}
