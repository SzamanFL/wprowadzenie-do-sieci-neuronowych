﻿using System;
using System.Linq;

namespace Sieci2
{
    class Program
    {
        static void SetArray (ref double[,] vectors)
        {
            Array.Clear(vectors,0,vectors.Length);
            vectors[0, 6] = 1;
            vectors[0, 7] = 1;
            vectors[0, 12] = 1;
            vectors[0, 17] = 1;
            vectors[0, 22] = 1;
            vectors[0, 25] = 1;

            vectors[1, 2] = 1;
            vectors[1, 3] = 1;
            vectors[1, 8] = 1;
            vectors[1, 25] = 1;

            vectors[2, 5] = 1;
            vectors[2, 6] = 1;
            vectors[2, 11] = 1;
            vectors[2, 16] = 1;
            vectors[2, 21] = 1;
            vectors[2, 25] = 1;

            vectors[3, 6] = 1;
            vectors[3, 7] = 1;
            vectors[3, 8] = 1;
            vectors[3, 11] = 1;
            vectors[3, 13] = 1;
            vectors[3, 16] = 1;
            vectors[3, 17] = 1;
            vectors[3, 18] = 1;
            vectors[3, 25] = 1;

            vectors[4, 10] = 1;
            vectors[4, 11] = 1;
            vectors[4, 12] = 1;
            vectors[4, 15] = 1;
            vectors[4, 17] = 1;
            vectors[4, 20] = 1;
            vectors[4, 21] = 1;
            vectors[4, 22] = 1;
            vectors[4, 25] = 1;
        }

        static double TeacherFunction(int t)
        {
            if ((t - 1) % 5 < 3)
                return 1.0;
            return 0.0;
        }

        static double ActivationFunction(double[] weights, double[,] vectors, int t)
        {
            double sum = 0;
            for (int i = 0; i < 26; i++)
                sum += weights[i] * vectors[(t - 1) % 5, i];
            if (sum >= 0)
                return 1.0;
            return 0.0;
        }

        static void Teaching(double z, double y, ref double[] weights, double[,] vectors, double c, int t, ref int counter)
        {
            for (int j = 0; j < 26; j++)
                weights[j] += c * (z - y) * vectors[(t - 1) % 5,j];

            if (z == y)
                counter += 1;
            else
                counter = 0;
        }

        static void Main(string[] args)
        {
            double[,] vectors = new double[5,26];
            SetArray(ref vectors);
            double[] weights = Enumerable.Repeat(1.0, 26).ToArray();
            int times = 1;
            int counter = 0;
            int c_option = 0;
            double c = 0;
            while (true)
            {
                weights = Enumerable.Repeat(1.0, 26).ToArray();
                times = 1;
                counter = c_option = 0;
                c = 0;
                Console.WriteLine("\nChoose value of 'C' \n\t0 for c=1\n\t1 for c=0.1\n\t2 for c=0.01");
                c_option = int.Parse(Console.ReadLine());
                if (c_option == 0)
                    c = 1;
                else if (c_option == 1)
                    c = 0.1;
                else
                    c = 0.01;
                
                while (counter != 5)
                {
                    double z = TeacherFunction(times);
                    double y = ActivationFunction(weights, vectors, times);
                    Teaching(z, y, ref weights, vectors, c, times, ref counter);
                    times += 1;
                }

                Console.WriteLine($"This is t = {times}");
                foreach (double item in weights)
                    Console.Write($"{item} ");
            }
        }
    }
}
