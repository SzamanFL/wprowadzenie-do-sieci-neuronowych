﻿using System;
using System.Collections.Generic;

namespace Sieci1
{
    class Program
    {
        public enum GateType { NOT, AND, NAND, OR, DEFAULT }

        public static void Gates(GateType gateType, List<double> inputs)
        {
            switch (gateType)
            {
                case GateType.NOT: // weights -0.5 0.33
                    {
                        double[] weights = new double[] { -0.5, 0.33 };
                        if (ActivationFunction(weights, inputs) >= 0)
                            Console.WriteLine("Output NOT = 1");
                        else
                            Console.WriteLine("Output NOT = 0");
                    }
                    break;
                case GateType.AND: // weights 0.33 0.33 -0.5
                    {
                        double[] weights = new double[] { 0.33, 0.33, -0.5 };
                        if (ActivationFunction(weights, inputs) >= 0)
                            Console.WriteLine("Output AND = 1");
                        else
                            Console.WriteLine("Output AND = 0");
                    }
                    break;
                case GateType.NAND: // weights -0.33 -0.33 0.5
                    {
                        double[] weights = new double[] { -0.33, -0.33, 0.55 };
                        if (ActivationFunction(weights, inputs) >= 0)
                            Console.WriteLine("Output NAND = 1");
                        else
                            Console.WriteLine("Output NAND = 0");
                    }
                    break;
                case GateType.OR: // weights 0.33 0.33 -0.33
                    {
                        double[] weights = new double[] { 0.33, 0.33, -0.33 };
                        if (ActivationFunction(weights, inputs) >= 0)
                            Console.WriteLine("Output OR = 1");
                        else
                            Console.WriteLine("Output OR = 0");
                    }
                    break;
            }
        }

        public static double ActivationFunction(double[] weights, List<double> inputs)
        {
            double sum = 0;
            inputs.Add(1);
            for (int i = 0; i < weights.Length; i++)
                sum += weights[i] * inputs[i];
            return sum;
        }

        static void Main(string[] args)
        {
            GateType gateType = GateType.DEFAULT;
            List<double> inputs = new List<double>();
            Console.WriteLine("Logic gates NOT, AND, OR, NAND using McCulloch-Pitts Neurons");
            while (true)
            {
                Console.WriteLine("Select logic gate\n\t0 - NOT\n\t1 - AND\n\t2 - NAND\n\t3 - OR");
                int type = Int32.Parse(Console.ReadLine());
                switch (type)
                {
                    case 0:
                        gateType = GateType.NOT;
                        break;
                    case 1:
                        gateType = GateType.AND;
                        break;
                    case 2:
                        gateType = GateType.NAND;
                        break;
                    case 3:
                        gateType = GateType.OR;
                        break;
                }
                Console.WriteLine("Give your inputs");
                try
                {
                    inputs.Add(Double.Parse(Console.ReadLine()));
                    if (!gateType.Equals(GateType.NOT))
                        inputs.Add(Double.Parse(Console.ReadLine()));
                    Gates(gateType, inputs);
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Input should be number");
                }
                inputs.Clear();
            }
        }
    }
}
